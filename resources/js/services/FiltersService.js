import api from './../services/api'

export default {
  fetchFilters () {
    return api().get('api/v01/filters')
  },
  addNewFilter (params) {
    return api().post('api/v01/filters', params)
  },
  updateFilter (params) {
    return api().put('api/v01/filters/' + params.id, params)
  },
  getFilter (params) {
    return api().get('api/v01/filters/' + params.id)
  },
  deleteFilter (id) {
    return api().delete('api/v01/filters/' + id)
  }
}
