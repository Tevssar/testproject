<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\FiltersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v01', 'middleware' => 'api'], function () {

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', [ProductsController::class, 'index']);
        Route::get('/{product}', [ProductsController::class, 'show']);
        Route::post('/', [ProductsController::class, 'store']);
        Route::put('/{product}', [ProductsController::class, 'update']);
        Route::delete('/{product}', [ProductsController::class, 'destroy']);
    });

    Route::group(['prefix' => 'filters'], function () {
        Route::get('/', [FiltersController::class, 'index']);
        Route::get('/{filter}', [FiltersController::class, 'show']);
        Route::post('/', [FiltersController::class, 'store']);
        Route::put('/{filter}', [FiltersController::class, 'update']);
        Route::delete('/{filter}', [FiltersController::class, 'destroy']);
    });

});
