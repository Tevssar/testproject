<?php
/**
 * Model for filters
 *
 * @category Model
 * @package  App\Models
 * @author   Bogdan
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Products;

/**
 * [Description Filters]
 *
 * @property int $id
 * @property string $title
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Products[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Filters newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Filters newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Filters query()
 * @method static \Illuminate\Database\Eloquent\Builder|Filters whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Filters whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Filters whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Filters whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Filters whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Filters extends Model
{
    use HasFactory;

    protected $fillable = ['title'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    /**
     * @return HasMany|Collection|Products[]
     */
    public function products()
    {
        return $this->hasMany(Products::class);
    }

}
