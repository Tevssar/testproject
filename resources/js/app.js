/**
 * Vue applicaton
 */

window.Vue = require('vue');

import './bootstrap';
import AppComponent from './components/AppComponent'
import router from './router'


const app = new Vue({
    el: '#app',
    router,
    template: '<AppComponent/>',
    components: { AppComponent }
});
