import api from './../services/api'

export default {
  fetchProducts () {
    return api().get('api/v01/products')
  },
  addNewProduct (params) {
    return api().post('api/v01/products', params)
  },
  updateProducts (params) {
    return api().put('api/v01/products/' + params.id, params)
  },
  getProducts (params) {
    return api().get('api/v01/products/' + params.id)
  },
  deleteProducts (id) {
    return api().delete('api/v01/products/' + id)
  }
}
