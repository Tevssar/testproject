Test application for giftbasketsoverseas

Installation:
Set `.env` file, `MIX_API_URL` - API URL for vue.js, currently same as `APP_URL`.
    `composer install`
    `npm install`
    `php artisan migrate:fresh`
    `php artisan db:seed`
    `npm run dev | npm run production`
