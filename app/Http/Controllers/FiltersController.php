<?php
/**
 * Controller for API interaction with Filters
 *
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Bogdan
 */
namespace App\Http\Controllers;

use App\Models\Filters;
use Illuminate\Http\Request;

/**
 * Controller for API interaction with Filters
 */
class FiltersController extends Controller
{
    /**
     * Return all filters.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Filters::all();
    }

    /**
     * Store a newly created filter in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Filters::create($request);
    }

    /**
     * Return the specified filter.
     *
     * @param  \App\Models\Filters  $filters
     * @return \Illuminate\Http\Response
     */
    public function show(Filters $filters)
    {
        return $filters->first();
    }

    /**
     * Update the specified filter in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Filters  $filters
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Filters $filters)
    {
        $filter = $filters->first();
        $filter->fill($request->except(['id']));
        $filter->save();
        return response()->json($filter);
    }

    /**
     * Remove the specified filter from storage.
     *
     * @param  \App\Models\Filters  $filters
     * @return \Illuminate\Http\Response
     */
    public function destroy(Filters $filters)
    {
        $product = $filters->first();
        if ($product->delete()) {
            return response(null, 204);
        }
    }
}
