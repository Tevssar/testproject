<?php
/**
 * Controller for API interaction with Products
 *
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Bogdan
 */
namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;

/**
 * Controller for API interaction with Products
 */
class ProductsController extends Controller
{
    /**
     * Return all products and corresponding filter
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Products::with('filters')->get();
    }


    /**
     * Store a newly created product in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Products::create($request);
    }

    /**
     * Return the specified product.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        return $products->first();

    }

    /**
     * Update the specified product in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products)
    {
        $product = $products->first();
        $product->fill($request->except(['id']));
        $product->save();
        return response()->json($product);
    }

    /**
     * Remove the specified product from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products)
    {
        $product = $products->first();
        if ($product->delete()) {
            return response(null, 204);
        }
    }
}
